import java.util.Arrays;
import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) throws Exception {

        Scanner sc = new Scanner(System.in);
        //while (true) {   //����, ��� ��������
            String task = sc.nextLine();
            if (isValidOperation(task)) {
                System.out.println(calc(task));
            } else {
                throw new Exception("throws Exception");
            }
        //}
    }

    /**
     * ��������� ��������� �������� ��� �������� ��� ������������ ��� ����������� ������
     * @param operation ����� ��������� �������
     * @return �������� - true
     */
    public static boolean isValidOperation(String operation) {

        String regex = "^\\s*([1-9]|10|[IVX]{1,4})\\s*([+\\-*/])\\s*([1-9]|10|[IVX]{1,4})\\s*$";
        return operation.matches(regex);
    }

    /**
     * ����������� ������� ��������� � ���������� �����
     * @param input ������ �������: ����� ������ �������� ������ �����, ����� �������� ��� �������, ��� �� ��������� ����� 10
     * @return ����� �������
     */
    public static String calc(String input) throws Exception {

        char[] arrayChar = input.toCharArray();
        Example ex = parse(arrayChar);

        return ex.calculate();
    }

    /**
     * �������������� ������ � �������������� ������
     * @param arrayChar ���������� ������ ��������
     * @return ����� � ������ ���� � ��������
     */
    private static Example parse(char[] arrayChar) throws Exception {
        int a=-1,b=-1, //����� � ������������� ����������
                count=0; //������� ����������� ������ �����
        char x=0; //������ ��������
        boolean romanFlag = false; // ���� ������� �����
        for (int i = 0; i < arrayChar.length; i++) {// ������� ������ �������� � ������� �������� ...
            if (arrayChar[i] == 32) { // ... � ������� ��������
                if (count == 1) { //������ ������ - ������ ����� ���� ������� ������ �����
                    if (arrayChar[i + 1] >= 73 != romanFlag) throw new Exception("throws Exception"); //�������� ��� �� ����� ������ �������
                    b = digit(Arrays.copyOfRange(arrayChar, i + 1, arrayChar.length)); // �������� ����� � �����
                    count++;
                    continue;
                }
                if (count == 0) { //������ ������ - ������ �� ���� ���� ������� ������ �����
                    romanFlag = arrayChar[i - 1] >= 73; // ���� ������� �����
                    a = digit(Arrays.copyOfRange(arrayChar, 0, i));  // �������� ����� � �����
                    x = arrayChar[i+1]; //������ ��������
                    count++;
                }
            }

        }
        //������ ��� ��������� ��� ������ �� ������ �������� �������
        return new Example(a,b,x,romanFlag);
    }

    /**
     * �������������� �������� ���� � �����
     * @param chars ������ �������� ����
     * @return �����
     */
    private static int digit(char[] chars) {

        char[][][] romanNumerals = new char[][][]{ //�������� ������� �� ������, ��������� ��� ����������� �������� �������� ���� ����������
                {{'I'},{'V'},{'X'}},
                {{'I','I'},{'I','V'},{'V','I'},{'I','X'}},
                {{'I','I','I'},{'V','I','I'}},
                {{'V','I','I','I'}}
        };

        int x=0;
        if(chars[0]>=73){//�������� �� �� ��� ����� �������
            //������ �� ������ �������� ������ ������������ ��������
            if(chars.length==1){
                if (Arrays.equals(chars,romanNumerals[0][0]) ){x=1;}
                if (Arrays.equals(chars,romanNumerals[0][1]) ){x=5;}
                if (Arrays.equals(chars,romanNumerals[0][2]) ){x=10;}
            }
            if(chars.length==2){
                if (Arrays.equals(chars,romanNumerals[1][0]) ){x=2;}
                if (Arrays.equals(chars,romanNumerals[1][1]) ){x=4;}
                if (Arrays.equals(chars,romanNumerals[1][2]) ){x=6;}
                if (Arrays.equals(chars,romanNumerals[1][3]) ){x=9;}
            }
            if(chars.length==3){
                if (Arrays.equals(chars,romanNumerals[2][0]) ){x=3;}
                if (Arrays.equals(chars,romanNumerals[2][1]) ){x=7;}
            }
            if(chars.length==4){
                if (Arrays.equals(chars,romanNumerals[3][0]) ){x=8;}
            }

        } else{//���� ������� ��������, �� ��������� �����
            for (char aChar : chars) {
                x *= 10;
                x += (number(aChar));
            }
        }
        return x;
    }

    private static int number(char x) {//����� ��� ��� ������� �����.
        return x-48;
    }

    private static class Example {
        int a;
        int b;
        char x;
        boolean romanFlag;
        Example(int a, int b, char x, boolean romanFlag){
            this.a = a;
            this.b = b;
            this.x = x;
            this.romanFlag = romanFlag;
        }

        /**
         * ��� �����������
         * @return ����� ��������� ��������
         */
        String calculate() throws Exception {
            int number = switch (x) {
                case 43 -> a + b;
                case 45 -> a - b;
                case 42 -> a * b;
                case 47 -> a / b;
                default -> throw new IllegalArgumentException("Invalid operator");
            };
            if (romanFlag){
                if (number < 1){throw new Exception("throws Exception");}
                return arabic2Roman(number);
            }
            return ""+number;
        }

        /**
         * ������� �� �������� � �������
         * @param number ������� ����� �� 1 �� 100
         * @return ����� �������
         */
        private String arabic2Roman(int number) {

            char[][] result = new char[5][];
            int count = 0;
            char[][] tableOfRomanNumbers9 = new char[][]{
                    {'I'},{'I','I'},{'I','I','I'},
                    {'I','V'},{'V'},{'V','I'},{'V','I','I'},{'V','I','I','I'},
                    {'I','X'}
            };
            if(number==100) return "C"; // ������������ ��������
            if(number>89) {result[count] = new char[]{'X', 'C'}; number -= 90; count++;}
            if(number>49) {result[count] = new char[]{'L'};number -= 50; count++;}
            if(number>39) {result[count] = new char[]{'X','L'};number -= 40; count++;}
            while (number>9) {result[count] = new char[]{'X'};number -= 10; count++;}
            if(number>0) {result[count]=tableOfRomanNumbers9[number-1];}

            return char2String(result);
        }

        /**
         * ������� �������� � ������
         * @param charArray ������ �������� ������� ����
         * @return ������
         */
        private String char2String(char[][] charArray) {

            StringBuilder stringBuilder = new StringBuilder();
            for (char[] subArray : charArray) {
                if (subArray != null) stringBuilder.append(subArray);
            }
            return stringBuilder.toString();
        }
    }
}




